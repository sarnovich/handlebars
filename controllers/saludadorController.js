var saludador = require('../models/saludador');

function saludo (req, res) {
  var nombre = req.query.nombre;
  res.render('saludo', {message: saludador.saludar(nombre)});
}

function despedida (req, res) {
  var nombre = req.query.nombre;
  res.render('despedida', {message: saludador.saludar(nombre)});
}

module.exports = {
  saludo: saludo,
  despedida: despedida
};
